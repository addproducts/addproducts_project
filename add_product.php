<?php
session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add product</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <main class="overflow-hidden">
        <div class="container my-5">
            <div class="col-12">
                <?= isset($_SESSION['productExist']) ? $_SESSION['productExist'] : ''; ?>
            </div>
            <form action="app/services/dataProcess.php" method="post" id="product_form">
                <div class="">
                    <div class="float-start">
                        <h2>Add Product</h2>
                    </div>
                    <div class="header float-end mb-3">
                        <button class="btn btn-success" name="send" value="save">Save</button>
                        <button class="btn btn-danger" name="send" value="cancel" id='cancel'>Cancel</button>

                    </div>
                    <div style="clear:both " class="border border-top border-0  border-dark"></div>
                    <div>
                        <div class="col-10 col-md-6 my-5">
                            <div class="form-group my-3">
                                <label class="form-lable my-2">SKU</label>
                                <input type="text" name="sku" class="form-control" id="sku" >
                                <?= isset($_SESSION['vaildation']['sku']) ?  $_SESSION['vaildation']['sku']  : '';

                                ?>


                            </div>
                            <div class="form-group my-3">
                                <label class="form-lable my-2">name</label>
                                <input type="text" name="name" class="form-control" id="name" >
                                <?= isset($_SESSION['vaildation']['name']) ?  $_SESSION['vaildation']['name']  : "";
                                ?>
                            </div>
                            <div class="form-group my-3">
                                <label class="form-lable my-2">price</label>
                                <input type="number" name="price" class="form-control" id="price" >
                                <?= isset($_SESSION['vaildation']['price']) ?  $_SESSION['vaildation']['price']  : ''; ?>


                            </div>
                            <div class="form-group my-3">
                                <label class="form-lable my-2">Type switcher</label>
                                <select name="type_switcher" class="form-select" id="productType" onchange="newform()">
                                    <option value=""></option>
                                    <option value="dvd">DVD</option>
                                    <option value="book">Book</option>
                                    <option value="furniture">Furniture</option>
                                </select>
                                <?= isset($_SESSION['vaildation']['type_switcher']) ?  $_SESSION['vaildation']['type_switcher']  : '';

                                ?>
                            </div>
                        </div>
                        <div id="selection">
                            <section class="col-10 col-md-6 p-3 border my-4 " id="dvd">
                                <label class="form-label my-2">Size(MB)</label>
                                <input type="number" name="size" class="form-control" id="size">
                                <?= isset($_SESSION['vaildation']['size']) ? $_SESSION['vaildation']['size'] : '';
                                ?>
                            </section>

                            <section class="col-10 col-md-6 p-3 border my-4 " id="book">
                                <label class="form-label my-2">Weight(KG)</label>
                                <input type="number" name="weight" class="form-control" id="weight">
                                <?=
                                isset($_SESSION['vaildation']['weight']) ? $_SESSION['vaildation']['weight'] : '';

                                ?>
                            </section>
                            <section class="col-10 col-md-6 p-3 border my-4 " id="furniture">
                                <div>
                                    <label class="form-label my-2">Height(CM)</label>
                                    <input type="number" name="height" class="form-control" id="height">
                                    <?= isset($_SESSION['vaildation']['height']) ? $_SESSION['vaildation']['height'] : '';

                                    ?>
                                </div>
                                <div>
                                    <label class="form-label my-2">Width(CM)</label>
                                    <input type="number" name="width" class="form-control" id="width">
                                    <?= isset($_SESSION['vaildation']['width']) ? $_SESSION['vaildation']['width'] : '';

                                    ?>
                                </div>
                                <div>
                                    <label class="form-label my-2">Length(CM)</label>
                                    <input type="number" name="length" class="form-control" id="length">
                                    <?= isset($_SESSION['vaildation']['length']) ? $_SESSION['vaildation']['length'] : '';

                                    ?>
                                </div>
                            </section>
                        </div>
                    </div>

                </div>

            </form>
        </div>
        <footer class="text-center border  p-2">
            sacndiweb tast assingment
        </footer>

    </main>
    <style>
        section {
            display: none;
        }
    </style>
    <script src="assets/js/main.js"></script>


</body>

</html>
<?php

?>