<?php
// this file working on deleted the products

use App\Database\ProductsTypes\Book;

include_once "../database/ProductsTypes/Book.php";
$product = new Book;

if ($_POST) {
    if ($_POST['page'] === 'add') {
        header('location:../../add_product.php');
    } elseif ($_POST['page'] === 'delete') {
        unset($_POST['page']);
        $product->setSku($_POST);
        $product->delete();
        header('location:../../index.php');
    
    }
}
