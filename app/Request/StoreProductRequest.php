<?php

namespace App\Request;

include_once __DIR__ . "/../database/Models/Product.php";

use App\Database\Models\Product;

session_start();
class StoreProductRequest extends Product
{
    public  $vaildation = [];

    public function productExist()
    {
        $query = "SELECT * FROM `products` WHERE `sku` = '{$this->sku}'";
        $resault =  $this->runDQL($query);
        if (!empty($resault)) {
            StoreProductRequest::redirectToAddPage();
            return "<div class='alert alert-danger  text-center mb-5'>The product containing this sku : {$this->sku} already exists</div>";
        }
    }

    public function requiredFields(array $fields)
    {
        foreach ($fields as $key => $value) {
            $key2 = str_replace("_", " ", $key);
            if (empty($value)) {
                $this->vaildation[$key] = "<div class='alert alert-danger'>The {$key} is required</div>";
                StoreProductRequest::redirectToAddPage();

                if (is_numeric(strpos($key, "_"))) {
                    $this->vaildation[$key] = "<div class='alert alert-danger'> The {$key2} is required</div>";
                    StoreProductRequest::redirectToAddPage();
                }
            }
        }
    }

    public function checkValues($value, $field)
    {

        if (!is_numeric($value)) {
            StoreProductRequest::redirectToAddPage();
            $this->vaildation[$field] = "<div class='alert alert-danger'> The {$field} requires a number </div>";
        } elseif ($value <= 0) {
            StoreProductRequest::redirectToAddPage();
            $this->vaildation[$field] = "<div class='alert alert-danger'>  It must be {$field} greater than 0</div>";
        }
        return $this;
    }
    
    public static function  redirectToAddPage()
    {
        return  header("Location:../../add_product.php");
    }

    public static function  redirectToHomePage()
    {
        return  header("Location:../../index.php");
    }
}
