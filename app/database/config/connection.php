<?php

namespace App\Database\Config;

use mysqli;
//the class working of the connection Database
abstract class Connection
{
    private $DBName = 'notion';
    private $password = '';
    private $userName = 'root';
    private $hostName = 'localhost';
    private $connection;

    public function __construct()
    {
        $this->connection = new mysqli($this->hostName, $this->userName, $this->password, $this->DBName);
    }
    // this is function working of the some opretions Of which(update , create , delete) 
    public function runDML($query)
    {
        $reuslt = $this->connection->query($query);
        if ($reuslt) {
            return false;
        } else {
            return true;
        }
    }
    // this is function working of the some opretions Of which(selected of querys) 

    public function runDQL($query)
    {
        $reuslt = $this->connection->query($query);
        if ($reuslt->num_rows > 0) {
            return $reuslt;
        } else {
            return [];
        }
    }
}
