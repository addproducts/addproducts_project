<?php

namespace  App\Database\ProductsTypes;

include_once __DIR__ . "/../Models/Product.php";
include_once __DIR__ . "/../config/Crad.php";

use App\Database\Models\Product;
use App\Cnofig\Crad;



class Dvd extends Product
{
    use  Crad;
    private $size;

    /**
     * Get the value of size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set the value of size
     *
     * @return  self
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }




    public function createValue()
    {
        $query = "INSERT INTO `product_spec` (`product_sku`, `spec_name`, `value`) VALUES 
        ( '$this->sku','size','$this->size')";
        return $this->runDML($query);
    }


    // selected to products contain type_switcher == DVD
    public function select()
    {
        $query = "SELECT
           products.*,
           product_spec.*
       FROM
           `products`
       JOIN product_spec ON products.sku = product_spec.product_sku
       WHERE
           product_spec.spec_name IN('size') ORDER BY products.created_at";
        return $this->runDQL($query);
    }


}
