<?php

namespace App\Database\ProductsTypes;

include_once __DIR__ . "/../Models/Product.php";
include_once __DIR__ . "/../config/Crad.php";

use App\Database\Models\Product;
use App\Cnofig\Crad;





class Book extends Product
{
    use Crad;

    private $weight;

    /**
     * Get the value of weight
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set the value of weight
     *
     * @return  self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }



    public function createValue()
    {
        $query = "INSERT INTO `product_spec` (`product_sku`, `spec_name`, `value`) VALUES 
        ( '$this->sku','weight','$this->weight')";
        return $this->runDML($query);
    }

    //  selected to products contain type_switcher == book


    public function select()
    {
        $query = "SELECT
            products.*,
            product_spec.*
        FROM
            `products`
        JOIN product_spec ON products.sku = product_spec.product_sku
        WHERE
            product_spec.spec_name IN('weight') ORDER BY products.created_at";
        return $this->runDQL($query);
    }

 
}
