<?php

namespace  App\Database\ProductsTypes;

include_once __DIR__ . "/../Models/Product.php";
include_once __DIR__ . "/../config/Crad.php";


use App\Cnofig\Crad;
use App\Database\Models\Product;

class Furniture extends Product
{
    use Crad;

    private $width;
    private $height;
    private $length;

    /**
     * Get the value of width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set the value of width
     *
     * @return  self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get the value of height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set the value of height
     *
     * @return  self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get the value of length
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set the value of length
     *
     * @return  self
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }


    public function createValue()
    {
        $query = "INSERT INTO `product_spec` (`product_sku`, `spec_name`, `value`) VALUES 
        ( '$this->sku','width','$this->width'),
        ( '$this->sku','height','$this->height'),
        ( '$this->sku','length','$this->length')";
        return $this->runDML($query);
    }


    //selected to products contain type_switcher == furniture

    public function select()
    {
        $query = "SELECT
           sku,
           product_sku,
           price,
           `name`,
           type_switcher,
           GROUP_CONCAT(`spec_name`),
           GROUP_CONCAT(`value` SEPARATOR 'x') as `values`
       FROM
           `product_spec`
       JOIN products ON products.sku = product_spec.product_sku
       WHERE
           product_spec.spec_name IN('length', 'width', 'height')
       GROUP BY
           product_sku";
        return $this->runDQL($query);
    }


}
