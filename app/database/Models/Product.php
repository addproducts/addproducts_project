<?php

namespace App\Database\Models;

include_once __DIR__ . "/../config/connection.php";

use App\Database\Config\Connection;

abstract class Product extends Connection
{
    protected $sku;
    protected $name;
    protected $price;
    protected $typeSwitcher;

    /**
     * Get the value of sku
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @return  self
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of TypeSwitcher
     */
    public function getTypeSwitcher()
    {
        return $this->TypeSwitcher;
    }

    /**
     * Set the value of TypeSwitcher
     *
     * @return  self
     */
    public function setTypeSwitcher($typeSwitcher)
    {
        $this->typeSwitcher = $typeSwitcher;

        return $this;
    }
}
