<?php
include_once "../Request/StoreProductRequest.php";
include_once "../database/ProductsTypes/Book.php";
include_once "../database/ProductsTypes/Dvd.php";
include_once "../database/ProductsTypes/Furniture.php";

use App\Database\ProductsTypes\Book;
use App\Database\ProductsTypes\Dvd;
use App\Database\ProductsTypes\Furniture;
use App\Request\StoreProductRequest;

$request = new StoreProductRequest;
$productOfDvd = new Dvd;
$productOfBook = new Book;
$productOfFurniture = new Furniture;


    $fields = ['sku' => $_POST['sku'], 'name' => $_POST['name'], 'price' => $_POST['price'], 'type_switcher' => $_POST['type_switcher']];
    $_POST['type_switcher'] === 'dvd' ? $fields['size'] = $_POST['size'] : '';
    $_POST['type_switcher'] === 'dvd' ?  $request->checkValues($_POST['size'], 'size') : '';

    $_POST['type_switcher'] === 'book' ? $fields['weight'] = $_POST['weight'] : '';
    $_POST['type_switcher'] === 'book' ?  $request->checkValues($_POST['weight'], 'weight') : '';

    $_POST['type_switcher'] === 'furniture' ? $fields = array_merge($fields, ['width' => $_POST['width'], 'height' => $_POST['height'], 'length' => $_POST['length']]) : '';
    $_POST['type_switcher'] === 'furniture' ? $request->checkValues($_POST['width'], 'width')->checkValues($_POST['height'], 'height')->checkValues($_POST['length'], 'length') : '';

    $request->checkValues($_POST['price'], 'price');
    $request->requiredFields($fields);
    $_SESSION['vaildation'] = $request->vaildation;

    if (empty($request->vaildation)) {
        $request->setSku($_POST['sku']);
        $productExist =  $request->productExist();
        $_SESSION['productExist'] = $productExist;

        if (empty($productExist) && $_POST['send'] === 'save') {
            if ($_POST['type_switcher'] === 'dvd') {
                $productOfDvd->setSku($_POST['sku'])
                    ->setName($_POST['name'])
                    ->setTypeSwitcher($_POST['type_switcher'])
                    ->setPrice($_POST['price']);
                $productOfDvd->create();
                $productOfDvd->setSize($_POST['size'])->createValue();
                StoreProductRequest::redirectToHomePage();
            } elseif ($_POST['type_switcher'] === 'book') {
                $productOfBook->setSku($_POST['sku'])
                    ->setName($_POST['name'])
                    ->setTypeSwitcher($_POST['type_switcher'])
                    ->setPrice($_POST['price']);
                $productOfBook->create();
                $productOfBook->setWeight($_POST['weight'])->createValue();
                StoreProductRequest::redirectToHomePage();
            } elseif ($_POST['type_switcher'] === 'furniture') {
                $productOfFurniture->setSku($_POST['sku'])
                    ->setName($_POST['name'])
                    ->setTypeSwitcher($_POST['type_switcher'])
                    ->setPrice($_POST['price']);
                $productOfFurniture->create();
                $productOfFurniture->setWidth($_POST['width'])->setHeight($_POST['height'])->setLength($_POST['length'])->createValue();
                StoreProductRequest::redirectToHomePage();
            }
        }
    }




    if ($_POST['send'] === 'cancel') {
        StoreProductRequest::redirectToHomePage();
    }

