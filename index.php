<?php
include_once "app/database/ProductsTypes/Book.php";
include_once "app/database/ProductsTypes/Dvd.php";
include_once "app/database/ProductsTypes/Furniture.php";


use App\Database\ProductsTypes\Dvd;
use App\Database\ProductsTypes\Book;
use App\Database\ProductsTypes\Furniture;


$productsOfDvd = new Dvd;
$productsOfBook = new Book;
$productsOfFurniture = new Furniture;

$DVD = $productsOfDvd->select();
$books = $productsOfBook->select();
$furniture = $productsOfFurniture->select();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Products</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <main class="overflow-hidden">
        <div class="container my-5">
            <form action="app/request/DeleteProductRequest.php" method="post">
                <div class="">
                    <div class="float-start">
                        <h2>Product List</h2>
                    </div>
                    <div class="header float-end mb-3">
                        <button value="add" name="page" class="btn btn-success">ADD</button>
                        <button value="delete" name="page" class="btn btn-danger" id="delete-product-btn">MASS DELETE</button>
                    </div>

                    <div style="clear:both " class="border border-top border-0  border-dark"></div>


                    <div>
                        <div class="row  my-5">

                            <?php
                            foreach ($DVD as $in => $value) {

                            ?>
                                <section class="col col-sm-5 col-md-4 col-lg-3 col-xl-2 m-3 border border-1 border-dark p-3">

                                    <ul class=" list-unstyled overflow-hidden ">
                                        <li> <input type="checkbox" name="<?= $value['sku'] ?>" value="<?= $value['sku'] ?>" class="form-check"></li>
                                        <li><?= "SKU : " . $value['sku'] ?></li>
                                        <li><?= "NAME : " . $value['name'] ?></li>
                                        <li><?= "PRICE : " . $value['price'] . " $" ?></li>
                                        <li><?= $value['spec_name'] . " : " . $value['value'] . "MG" ?></li>

                                    </ul>
                                </section>


                            <?php
                            }

                            ?>

                        </div>
                        <div class="row  my-5">
                            <?php
                            foreach ($books as $in => $value) {

                            ?>
                                <section class="col col-sm-5 col-md-4 col-lg-3 col-xl-2 m-3 border border-1 border-dark p-3">

                                    <ul class=" list-unstyled overflow-hidden ">
                                        <li> <input type="checkbox" name="<?= $value['sku'] ?>" value="<?= $value['sku'] ?>" class="form-check"></li>
                                        <li><?= "SKU : " . $value['sku'] ?></li>
                                        <li><?= "NAME : " . $value['name'] ?></li>
                                        <li><?= "PRICE : " . $value['price'] . " $" ?></li>
                                        <li><?= $value['spec_name'] . " : " . $value['value'] . "KG" ?></li>


                                    </ul>
                                </section>


                            <?php
                            }

                            ?>


                        </div>
                        <div class="row  my-5">
                            <?php
                            foreach ($furniture as $in => $value) {

                            ?>
                                <section class="col col-sm-5 col-md-4 col-lg-3 col-xl-2 m-3 border border-1 border-dark p-3">

                                    <ul class=" list-unstyled overflow-hidden ">
                                        <li> <input type="checkbox" name="<?= $value['sku'] ?>" value="<?= $value['sku'] ?>" class="form-check"></li>
                                        <li><?= "SKU : " . $value['sku'] ?></li>
                                        <li><?= "NAME : " . $value['name'] ?></li>
                                        <li><?= "PRICE : " . $value['price'] . " $" ?></li>
                                        <li><?= "Dimension : " . $value['values'] ?></li>
                                    </ul>
                                </section>


                            <?php
                            }

                            ?>


                        </div>
                    </div>
                </div>

            </form>
        </div>
        <footer class="text-center border p-2">
            sacndiweb tast assingment
        </footer>

    </main>
    <script src="assets/js/main.js"></script>

</body>

</html>